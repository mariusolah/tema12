var tema = new Vue({
    el: '#tema',
    data: {
        array: [],
        squares: []
    },
    mounted: function(){
        let dsa = [];
        for(let i=0;i < 8;i++) {
            dsa.push('#'+(0x1000000+(Math.random())*0xffffff).toString(16).substr(1,6));
        };
        this.squares = dsa.concat(dsa);
        this.squares.sort(() => Math.random() -0.5);
    },
    methods: {
        click: ({ target }) => {
            tema.array.push(target.children[0]);
            tema.array.forEach(el => el.style.visibility = 'visible');
            if (tema.array.length === 2) {
                setTimeout(() => {
                    tema.array.forEach(el => el.style.visibility = 'hidden');
                    if (tema.array[0].style.backgroundColor === tema.array[1].style.backgroundColor) {
                        tema.array.forEach(el => {
                            el.parentNode.style.border = 'none';
                            el.style.display = 'none'
                        });
                    }
                    tema.array = [];
                }, 500);
            } 
        }   
    }
})